# The California Lighthouse<br />the Conceit

The California Lighthouse is a fake news company, a collection of
magical-realist journalism. Let's pick this apart, though, because that genre
definition will mean something different to different people.



## Magical realism

In this world, magical things happen every day --- but it's not exactly
commonplace. If someone were attacked by a werewolf, they would probably feel
shock because they're being attacked rather than because they were suddenly
faced with incontrovertible proof that werewolves exist; they would already know
that much. However, not everyone is a wizard. The magical is something that you
could largely miss if you weren't paying attention, or if you never visited
certain neighborhoods or wilderness areas. Supernatural elements will therefore
be like any other potentially interesting subject for an informative story:
things you might never have heard of; things you might be sorta familiar with
but not know a whole lot about; and things which you might *think* you know
pretty well, but which still has some surprises for you.

The inclusion of magic means that our stories can't be strictly "realistic," but
should mean that, despite whatever supernatural elements or fairytale or
mythological or high fantasy tropes you use, the story should otherwise feel
very "real." Your characters are real people, living in a modern world, and
dealing with all the things that go with that. For example, magic's
widespread-but-not-ubiquitous status means that access to healthcare may still
be an important issue for many people.



## Journalism

There are a few reasons why mixing journalism with magic could be an interesting
premise for a story. One is the tension inherent to wild understatement, the
dissonance between an exotic story written in mundane prose. Another is when
you're making an attempt at a sort of verisimilitude.

The voice-versus-content approach can lead to interesting dramatic or comedic
effect. When you have a crazy story to tell, sometimes a deadpan delivery will
elicit the strongest reaction. And Capital-J Journalism can also lend an air of
authority and gravitas which makes a silly story even funnier. This is kind of
the Onion's whole schtick. I don't mean that to be dismissive either --- it's
effective. So if that's what drives you, go for it.

Such stories tend to require a very specific writing style, though. If you
aren't as interested in writing like a dispatch from the Associated Press, then
fortunately, there's more than one kind of journalism, and more than one reason
to use a conceit like this.

Verisimilitude. Journalistic fiction at its best may be interesting because
there's never any doubt about who's telling you the story. Like the school of
thought in journalism that eschews the idea that perfect utter objectivity is
even possible (or useful), fiction written in first-person avoids the weird
questions that go with the Omniscient Third-Person Narrator. (Who is this
all-knowing narrator? Is it supposed to be God talking to us?) When the narrator
does not hide their identity, when the narrator becomes a regular
flesh-and-blood human being, telling you a story, it strengthens the "truth"
claims a work of fiction makes. And fiction is about telling the truth.

Just like journalism.

This second approach also lends itself to a broader range of prose styles. I
think there's room in the <cite>Lighthouse</cite> for a variety of voices. If
you have a hard time getting the AP style out of your head, try reading more
longform journalism? Or, maybe think of the California Lighthouse as a *blog*
instead.

If in doubt, [just tell the truth](/style-guide/stories).



## Time

Most stories published at a journalistic institution are about *recent* events.
Some exceptions include "then and now" stories, and other pieces which look back
at past events for some reason, perhaps on the anniversary of some victory or
tragedy.

Our format therefore lends itself to highly topical fiction --- which is all fine
and good. Who can resist a piece making fun of the creatures which roost in
Donald Trump's hair? However, the Lighthouse is not *merely* a collection of
topical humor. The best stories have something "timeless" about them or they
wouldn't be fondly remembered later as classics.

Anyway, the point is that if you want to write about something in the
not-so-recent past (or even the future?!?) you can. Just label the story with
the date on which it would have taken place, and get writing.
