# The California Lighthouse Style Guide<br />for Stories

If you haven't yet, [read the Conceit](/style-guide/conceit).


****


A journalist's job is nothing more or less than to tell the truth.

This by no means requires a cold and reserved prose style, nor does it preclude
admitting that the journalist was a participant in the story in some way.
Remember that the story is about the events you witnessed and the people you
met; but it is perfectly fine, and even desirable, for you to tell the story
vividly, as you saw it with your own eyes and heard it with your own ears; so
tell it in your own words. This is not a sterile science --- real science is not
often sterile, and the
[Copenhagen Interpretation](https://en.wikipedia.org/wiki/Schr%C3%B6dinger%27s_cat#Copenhagen_interpretation)
is never truer than in journalism --- so you will inevitably affect the
story you're recording. Use your natural voice. Just tell the truth.

Try to be succinct, but tell the whole story. Many push so hard for brevity
that their stories lose all sense of *narrative* --- wandering through your
story without ever finding a point is the sin of novices; lately, cutting
it too short is the sin of professionals. An unsorted heap of facts and
statistics does nobody any good without context. Any piece, no matter how
thorough or how brief, is a waste of the reader's time if it doesn't help
them see and understand the bigger picture. This is why we write stories
instead of merely compiling spreadsheets. Just tell the truth.

Some make too much of striving for objectivity, or "balance." Acknowledge
and quell your prejudices, acknowledge your conflicts of interest. In cases
where the truth is ambiguous, or uncertain, or subjective, you should be
honest about it. But there will be cases where two parties will try to tell
you (very!) different "facts," and if you don't pick a side, or stake out
your ground between the two, you will be doing a disservice to your
readers, who rely on you to cut through the bullshit for them. Just tell
the truth.

