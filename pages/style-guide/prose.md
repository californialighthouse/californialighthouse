# The California Lighthouse Style Guide<br />for Prose

We embrace a broad range of prose styles here at the Lighthouse. The notes here
are mostly about clarity rather than dictating a certain tone or voice for all
writing at the publication. So look here if you're ever unsure about something,
but feel free to stray from these guidelines *if it suits the story*.



## Names

Introduce people with their full name, and then refer to them by their surnames
after that. Exceptions to this would be for interviewees who decline to give
their full name, or for stories about multiple members of the same family.
People who wish to be anonymous may be called by their initials, or given
pseudonyms, such as Jane or John Doe, if referring to them without a name makes
the prose cumbersome.

Include job titles or other qualifications when the person is a subject matter
expert or if it's otherwise relevant to the story, but refrain from honorifics
afterward.

Some newspapers introduce citizens with their age when the person is unknown to
the general public, e.g., "Stanford Pines, 62." We don't need to explicitly
state people's ages --- or race, religion, and so on --- unless it's actually
pertinent to the story.



## Quotes

It's important to get the quotes right, and to be clear at all times who is
saying what. Introduce the speaker *before* the quote.

> ✘ "Now, I'm not racist, but...," said Donald Trump.

> ✔ Donald Trump said "Now, I'm not racist, but..."

Also, note that because the ellipsis (...) formally indicates omission of
part of a quote, it should **not** be used to indicate a hesitation or
other pauses in speech; for that use dashes instead. Both dashes and
ellipses in quotes should be used sparingly, though.

When you do omit part of a quote, wrap the ellipsis in square brackets, with white space on either side. If the ellipsis falls at the point where you want to end your sentence, put the period directly after the closing bracket.

<!-- TODO: need better examples here. -->
> He said "Quote quote [...] quote quote."

> He said "Quote quote quote [...]."

Quotation marks *before* the period or comma, unless the punctuation is part of the quote?



## Markup

Stories are stored in the website's git repository as Markdown files. So the
easiest way for us to receive submissions would actually be a "merge request" on
[GitLab](https://gitlab.com/californialighthouse/californialighthouse).
However, GitLab is geared more toward programmers, and most writers aren't
likely to know [how to use git](https://git-scm.com/documentation),
or all the markup code, so we'll accept regular Google Docs submissions too.

But for the authors with passing familiarity with Markdown or HTML, you
could make our lives easier if you format your submissions from the
beginning. See the coding style guide, particularly the section about
[markup and text-level semantics](/style-guide/code#markup-and-text-level-semantics).



## Citations

We do not use parenthetical or footnote styles of citation. (Not that you're
forbidden from using parentheses for this purpose; just don't do it like an MLA
or APA essay.) If your piece builds on the work of another reporter or quotes
some other work, you should build the citation into your prose; provide the
author, title, publisher, publication date, and/or hyperlinks as appropriate.
You should always state the source's reporter/author, and preferably it should
be clear from your prose alone which work you are referring to, but you may
elide some of the rest of the information if you can provide a direct link to
the cited work.
