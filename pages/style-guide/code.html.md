# The California Lighthouse Style Guide<br />for Code

## for all code

All text files should be encoded as UTF-8.

Brevity usually helps clarity. Fewer words and fewer digressions lead to
fewer distractions, and hopefully, fewer opportunities for confusion.
However, code is for humans, or else we would be writing in binary, so we
generally want to emphasize clarity above all other things. Clever hacks
aren't as important, speed often isn't something to worry about unless or
until we know the program runs *too slow*, and extreme terse code can make
the code worse rather than better.

Remember that these are generalizations, though, and the rules are enforced
or not on a case-by-case basis.

Whitespace makes things easier to read. Use it liberally.

Use plenty of code comments, preferring complete sentences, to help explain
your logic. Put comments on their own line(s) *before* the code that they
help explain. For longer files, comments may also be used to create a sort
of table of contents at the beginning and section headings throughout.

Indent with tabs, not spaces. However, horizontally spaced items will need
to use spaces (after the tab indentation).

Prefer double-quotes.

Lines generally shouldn't run longer than 80 characters.


## git

Try to keep your commits fairly atomic: each commit should generally be
focused on one concept that you want to implement, or one bug to fix. Use
your judgment here, though --- fixing multiple typos can go together in a
single commit, but it would be nice to have fixing typos separate from
implementing a new feature.

The first line in a commit message should be a brief summary of what is
happening here. Starting with a verb in base form may be a good way to keep
this line short. After that first line, we highly encourage you to explain
your reasoning in more detail, as appropriate. We can check the diffs to
see what changes you made, but for subtle or complex changes, it may not
always be immediately clear what your intentions were. However, there are
plenty of cases where the rationale should be obvious after the first line
of the commit message (e.g., "Fix misspellings.").


## Markup and text-level semantics

This section is relevant to document authors and editors just as much as
our web developers, because the principles of semantic markup very closely
ties the code to the content. The fact that our stories are generally
stored in the git repo as Markdown files helps simplify most markup, but
not all. Probably the trickiest thing for authors and editorial to remember
will be the way that web markup differentiates between the different
purposes for using italic and bold text:

* Italics are probably most commonly used to mark emphasized text. In this
	case, use an asterisk at the beginning and end (`*like this*`). When the
	Markdown gets compiled to HTML, it turns into the emphasis `<em>` tag.
	Emphasis may often change the connoted meaning of a phrase or sentence, by
	contradicting or moving around from one's expectations: For example, "I love
	*puppies*" would emphasize that the speaker loves puppies, as opposed to
	some other animal (perhaps they don't like cats?), while "I *love* puppies"
	stresses the feeling of love (as opposed to disgust or indifference). In
	written speech, emphasis may also indicate the speaker's strong feelings,
	but should be used minimally, or else dialog with a passionate speaker will
	turn into a mess of italics and exclamation points.
* Italics are also sometimes used to mark the titles of creative works;
	emphasis would not be appropriate for this case. Markdown does not have a
	shorthand for this, so we use the HTML `<cite>` element. For example, `I
	read <cite>Huckleberry Finn</cite> in high school.` We will use the
	`<cite>` tag for the titles of all works, not just long works like books
	or films.
* Italics for text from a foreign language uses a `lang` attribute on a `span`
	element. For example, `We shook hands and said "<span lang="es">Gusto en
	conocerlo.</span> Nice to meet you."` This has the added benefit of
	allowing text-to-speech readers to pronounce the foreign words correctly
	if they support the other language.
* Bold style is generally used to mark text as important in some way; use two
	asterisks at the beginning and end (`**like this**`). When the Markdown
	compiles to HTML, it turns into the `<strong>` tag. "Importance" here does
	not necessarily change meaning the way emphasis does; instead, you are
	just trying to draw the reader's attention to make sure they do not miss
	an important detail. For example, **do not skip this sentence**.
* Important terms may be styled a few different ways. Here's what you need
	to do:
	* Abbreviations, acronyms, and initialisms use `<abbr>`. If the term is likely
		to be unfamiliar to most readers, you should explain it in a sentence
		when the term first appears. More widely used abbreviations can have be
		briefly explained in the `title` attribute, which will appear as hover
		text on the web page. For example, `Daniel Day-Lewis lives in the <abbr
		title="United Kingdom">UK</abbr>.`
	* Terminology is marked with `<dfn>` and should be defined when it first
		appears in an article. Less widely known terms should be defined in a
		sentence. More familiar terms may be defined in a title attribute the
		same way you would for abbreviations. `<dfn>Functional Magnetic Resonance
		Imaging (<abbr>fMRI</abbr>)</dfn> is the technology and techniques used
		by scientists to see the brain in action.`

The differences between these uses of italic and bold is important for users who
rely on text-to-speech software, because the program, just like a human, should
say emphasized text differently from the text in a movie title.

The rest should be pretty simple:

* Put a blank line between paragraphs and headings.
* Hyperlinks look like this: `[click here](http://example.com)`.
* Headings start with the pound sign, or "hash tag" (`#`). The top-level
	heading, is the title of your story and there should only be one per
	document; generally this will be genereated by the `title` attribute in your
	YAML frontmatter. Subheadings mark subsections and use more hash tags, so
	the second-level headings start with two (`##`), subheadings within that
	section use three, and so on.

For Google Docs, don't worry about line length.



## markdown

We use
[GitHub-Flavored Markdown](https://help.github.com/articles/github-flavored-markdown/),
which allows for somewhat more advanced formatting, such as simple tables.
But don't be afraid to embed actual HTML; documents should generate
correct, semantic markup. HTML inside a Markdown paragraph should be
written on a new line, unless it's a short span-level tag with no
attributes --- for example, `<kbd>`.

Prefer Markdown version of HTML character entities --- such as `(c)` instead of
`&copy;` --- *but* prefer the literal Unicode characters (`©`) over either, when
possible.

"Smart" punctuation, such as curly quotes and em dashes, are converted for
you when the Markdown document gets compiled to HTML, so you most of the
time, you don't need to worry about converting basic punctuation. The
Markdown compiler does **not** convert punctuation inside embedded HTML
tags, though. For such cases, look at the stories style guide, under the
[typography section](/style-guide/stories#typography),
and manually insert Unicode versions of any "smart" punctuation.

Hyperlinks and image tags should be kept on a single line. (Note: This is not
the rule for HTML, however.) If this runs long, put them on their own lines,
separate from the surrounding prose content, but don't split the link or image
markup. When continuing the prose after the link, though, commas and periods
that come directly after a hyperlink or inline HTML need to touch the closing
parenthesis of the link, to make sure the punctuation isn't floating in
whitespace.

``` markdown
Hey guys, check it out! Firefly was just renewed for a
[second season](http://seriously-for-reals-firefly-season2.biz/rick-astley-never-gonna-give-you-up.avi),
according to my legit sources.
```

Bullet points use asterisks (`*`), not hyphens.

Numbered lists use `1.` format --- longer lists or lists whose contents are more
likely to change would be better off keeping all list items starting with the
literal one-and-period `1.` but you may write the actual numbers for shorter and
more static lists. List items that run long should have the hard-wrapped
secondary lines indented one level relative to the first line.

``` markdown
My bucket list:

* get a pet gecko and name it Mozilla
* get a hair cut in Seville, Spain
* drop some cannon balls off the edge of the Leaning Tower of Pisa
* start a restaurant, and have one of the featured menu items be the Shirley
	Temple: a hot bowl of soup served with animal crackers
* Try [this](http://xkcd.com/941/) to get some perspective...
```



## html

Make your markup as semantic as possible! Text-level semantics are a big
part of composing documents, and so are covered in a separate section
above, but the principles of semantic markup also mean choosing meaningful
class and id names, using heading tags instead of the blunter method of
style attributes which make a span of text larger, and so on.

Prefer separation of structure, presentation, and behavior --- i.e., styles
and JavaScript generally shouldn't appear in the HTML.

Use XHTML-style self-closing tags: Add a trailing slash, separated from any
previous words by a space. E.g., `<br />`.

For character entities, prefer named entity references over numeric
references, but prefer the literal Unicode character over entity references
at all. Properly encoded Unicode characters should pretty much never be a
problem, except when you need characters which could be interpreted as HTML
markup --- basically, `<`, `>`, and `&`.

``` html
Escape your &lt;html&gt; tags.

Literal “curly quotes” and arrows → and so on are fine.
```

Tags which have a very long set of attributes should flow into a second or
more lines, and these extra lines should be indented one extra tab.

``` html
<a id="state-issued-photo-id" class="classless-utopia" rel="cite"
	href="https://really-long-url-example.coffee">
	Contained text doesn't need be indented again, though; syntax highlighting
	should make it easy enough to see.
</a>
```

Nested tags should indented on a new line, unless the elements have few
attributes and fit easily on a single line of markup. Text, such as inside
paragraphs and headings, should also be placed on new lines and indented, unless
the whole tag and its text content fit neatly onto a single line. Simple
span-level tags such as `<em>` don't need to be pulled into separate lines or
indented even if their contents split across multiple lines. The point here is
to improve readability, so use your judgment.

Commas and periods that come directly after a hyperlink or other span-level
element must *touch* the closing tag, to make sure the punctuation isn't
floating in whitespace.

``` html
I know it's true because I heard about it from
<a href="http://theonion.com">
	a credible journalistic institution
</a>.
```



## css

Semantic markup applies to CSS as well: classes and ids should be named for
the purpose or context that the styled elements will be used in, rather
than how you want them to look. Styles like `#float-left` won't make
sense if you later decide to adopt a single-column layout, for example,
while `#table-of-contents` will only stop being appropriate if you decide
to stop including a table of contents on a page.

Set color values using either a color keyword (e.g., `red`), or the `hsl()` or
`hsla()` function.

Keep related selectors --- such as alternate styles for a single class when
adjusted by media query --- as close together as you can.

The sequence of selectors in a stylesheet should roughly follow the order
you're likely to see them in the styled document.

For Stylus, you can omit semicolons and curly braces, but don't omit
colons.



## javascript

Never omit semicolons.

If-statements always use curly brackets, but very short statements may be
written on a single line.

``` javascript
if ( conditional ) { code; }
```

Round, square, and curly brackets always have whitespace around their
contents, unless the first character inside is another bracket. Empty
brackets don't need any spaces inside either.

``` javascript
( { foo: [ 1, 2, 3 ]})

var foo = {};
```

Opening parenthesis touches the name of a function, but not the `function`
keyword.

``` javascript
( function ( arg ) { ... })();

foo( "bar" );
```

Prefer the separation of structure, presentation, and behavior. Instead of
`onclick` and `onload` attributes in the HTML, prefer grabbing elements
with `getElementById` or some similar method, and attaching `click`,
`load`, or other events.

When breaking up a long set of boolean operators over multiple lines, separate
expressions with the boolean operator at the end of a line, instead of at the
beginning of the next line. Apply hanging indent to the broken line.

``` javascript
foo < bar && foo == "string" && foo > baz &&
	bar >= quux && typeof fizzbuzz == "boolean"
```
