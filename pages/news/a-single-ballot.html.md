---
title: "A Single Ballot"
subtitle: "The Truth Behind the California Election Process"
author: "Itzel Azarola"
contributor: "Natalie Nicoletta Florence"
date: "2010-11-02"
---

The suspicion that one's vote does not actually matter has long been the
favorite complaint of the politically disaffected and disillusioned.
However, since voter turn out for the last California gubernatorial
election was at an all-time low, it would seem that feelings of
disenfranchisement is at an all time high. In light of this, California
State officials decided to allow journalists to directly witness the
electoral process from beginning to end on voting day.

We (the authors of this article, along with reporters from a few other
major newspapers) gathered at the end of the day in San Francisco to meet
with Darrell Steinberg, President pro tempore of the California State
Senate. The tour began at a local polling booth, set up outside the Hyde
Street Post Office. We followed boxes of ballots to the California Supreme
Court headquarters, strolling through the night for the short block and a
half to get there. As we walked to the courthouse, Steinberg explained:

"As usual, people show up to the different voting locations throughout the
state and cast their ballots. And all of those ballots are indeed counted.
Rest assured on that --- each one. It would be quite a charade holding fake
elections just to give the people 'something to do'...

"However, tallying the votes and figuring out who or what got the most is
only all there is for the referendums and the lesser minor candidate
offices [sic]. For people running for office in the county level and
higher, there's an extra step, which I suppose is what you'll all be
interested in, so that's what I plan on showing you tonight... The extra step
isn't written into the published state constitution, but it has been part
of our tradition *since before 1849*" (emphasis his).

Steinberg cut open a box of ballots, lifted the first slip from the top. An
assistant checked the ballot stub number and looked up the original voter's
information. Steinberg went on, "We'll take this guy as an example: [NAME
REDACTED] would get to be our host in the gubernatorial selection seance.
We go to his house to, you know, inform him of the honor, even though they
usually don't remember much afterward..."

It used to be that the chosen medium was taken to a government building,
usually one of the courthouses, but Steinberg said they now find it easier
on everyone to hold the ritual in the chosen person's house, if they can
get a quiet room to themselves. The person who acts as medium for the
ritual could be anyone, Steinberg stressed. For higher offices, the
electoral district and even the ballot box number are randomly selected days
before, and when the boxes arrive to be counted, a particular ballot is
randomly chosen. This year, the governor was to be chosen from this
district of San Francisco, which is why we were invited here instead of
Sacramento.

Then they carry on with the seance and the Old Ones determine who shall be
governor – or state senator, or assemblyman, or whatever the particular
office may be.

At this point, one of the other reporters interrupted Steinberg's account
of the ritual itself to ask, "The 'old ones'? What are you talking about?"

Steinberg said "The Old Ones are the spirits who have occupied what is
today known as California since before the Ohlone Indians showed up. I
wouldn't say that they cared enough to have anything to do with the
changes, but they allowed the Spaniards to take the lands from the
indigenous peoples, and then allowed the Americans to claim the land from
Mexico, so we have strived to remain in their favor so they will allow us
to *remain* here..."

We then broke for a late dinner while we waited for the randomly designated
ballot box to arrive. Perhaps the other reporters also felt we would be
better served saving our questions for the end? There was very little
discussion as we stood around in a side room eating cold sandwiches,
without gusto.

Eventually the box arrived, and we gathered round with Steinberg again.
This time we were also accompanied by Chief Justice Tani Cantil-Sakauye of
the California Supreme Court. Cantil-Sakauye arrived a little late, so she
began immediately, opening the chosen box and dumping the ballots into a
large steel wire basket. She spun this to mix up the bundles of card stock,
and blindly pulled one from the pile. She read off the ballot stub number.
An assistant tracked down the associated name and address again, and we
took to the streets again, this time in 4 cars.

The person who acted as medium tonight asked to remain anonymous --- after
being persuaded to participate at all. It was about 2am when we arrived at
the medium's condo. Cantil-Sakauye knocked on the door and waited patiently
for the medium to answer, explained the "honor," crooning over the woman's
confused angry drowse. As soon as she agreed, the President pro tem and the
Chief Justice pushed their way in along with their attendants, who
efficiently cleared the furniture from the tiny living room to make more
room for the ceremony.

Steinberg held up a parchment scroll for reference as Cantil-Sakauye
painted red concentric circles and markings onto the hardwood floor, only
occasionally glancing up to make sure she'd gotten the complex symbols in
the right order. The state officials wouldn't allow any cameras out during
this process, saying photographic equipment could cause interference, but
painted on the floor, sprinkled among the more abstract symbols, were
dozens of eyes, glistening red. It smelled a little foul.

I said "That isn't paint, is it."

One of the attendants answered, "Coyote blood."

"Of course."

Patrick McGreevy of the LA Times snickered and said "Are you ready for the
inevitable PETA backlash when this goes to print?" but no one answered.

The medium finally began to feel uncomfortable, so when Cantil-Sakauye
asked her to take off her clothes so they could paint the final markings on
her body, the medium asked if they could do it in private in the bathroom.
She returned moments later, seeming strangely calm now. She was covered in
more of the markings used on the floor, with more eyes gazing out from her
forehead, her cheeks, and on her closed eyelids. When they laid her out in
the circles, in spite of her earlier anxiety, the medium fell asleep almost
immediately. She did not wake or struggle when Cantil-Sakauye squeezed a
hand over her throat for a full minute. The reporters grew antsy, watching
this. Twice I started to protest but Steinberg just said to be quiet. I was
about to throw the Chief Justice off the poor woman, when Cantil-Sakauye
suddenly stood, and the attendants all stood and started stomping their
feet, drumming their bellies, clapping their hands. The medium stirred a
little, but smiled serenely, her real eyes closed but her painted eyes
glaring, as Cantil-Sakauye stooped to draw blood from her ankles. The Chief
Justice used this fresh blood to redraw each of the eyes on the floor and
on the medium's body.

Just as she finished the last eye, the medium suddenly sat bolt upright.
The drumming ceased. She turned her head up toward the ceiling, but looked
out only with her dripping painted eyes. Sometimes they blinked. She turned
down, then moved to face each of us. She then stood, up on the very tip of
her toes like a practiced ballerina, and held that pose for 10 minutes. She
hummed. She was the only one singing, but there were multiple voices
ringing in her throat. Sometimes it sounded like someone laughing. She
turned back to Cantil-Sakauye, and spoke:

"Edmund Gerald Brown, called Jerry, son of Edmund Gerald Brown, called Pat.
Again."

Cantil-Sakauye said "Yes."

And the medium collapsed. One attendant dove to catch her before she hit
the floor, smearing coyote and human blood all over himself. He laid her
gently back to the floor. Another attendant fetched a pillow from the couch
they'd removed and placed it under her head. Another attendant rummaged
through the hallway closet and covered her in a blanket. Another attendant
took out a sponge, some cleaning solutions, and started to heat up a kettle
of water on the stove.

Steinberg then started to usher us toward the door. "We'll help her clean
up in a bit, but being possessed takes a lot out of you; best to just let
her rest for now." As soon as we were outside again, he agreed to take some
questions.

What happens with the medium, during and after? Is she okay?

Steinberg said "Well, you're welcome to ask her herself, though I haven't
seen many mediums feel too chatty afterward." She wasn't interested in
discussing it. "She's fine, though, they're always fine. A little woozy,
maybe, everything feels a little fuzzy till morning. They often don't
remember much the next day, but it isn't traumatic so much as, ah,
emotionally exhausting."

But really, what just happened in there?

"The Old Ones are powerful – the Great Invisible Hand, they used to be
called, till we realized there was more than one – but they're, uh,
incorporeal, see? No mouth, no vocal cords. So to communicate, we need to
provide them with a 'mouthpiece'."

Right, right. So who really are the Old Ones? What are they? Are they good
or bad? How the hell did this tradition get started? Why?

Steinberg smiled at the barrage of questions growing louder. "We're not
entirely sure where the spirits come from, or who they are, or if 'who' is
the right word. We're pretty confident that they're not devils or demons,
or anything like that, though. No brimstone or sacrifices, see? No, they're
definitely, most certainly a friendly force."

Steinberg tried to cut off the discussion after this, repeatedly saying
that it was getting late, and promising a press conference sometime "in the
next week or two, probably." However, as we rode back to the courthouse, I
thought of one more question, which the President pro tem finally decided
to answer: Are there any rituals like this outside of California?

"Ah, yes, actually. This isn't exactly an unusual thing. There's a similar
seance for most of the continental United States, at both the state and
often the federal level. The US Presidential election is actually the odd
one out, as the whole business with the electoral college and that works
exactly the way you learn it in your high school government classes. There,
it just doesn't matter, since either candidate will end up serving the Old
Ones anyway."

As of publication time, there has not yet been a confirmed date for a press
conference concerning the seances.
