# the California Lighthouse

These are the source files for generating the magical-realist journalism
website, [California Lighthouse](http://californialighthouse.org), using
[DocPad](http://docpad.org/).

Unless noted otherwise, everything here is published under the Creative Commons
Attribution-ShareAlike 4.0 International License. Meaning basically that you're
free to copy, share, and remix these works, as long as you "share alike." See
the full license for details --- either here in the `LICENSE.md` file, or
[online at CreateCommons.org](https://creativecommons.org/licenses/by-sa/4.0/).
Any exceptions to this license will be noted at the bottom of each article.

You're also welcome to contribute! File issues against to this repository, or
email [editorial@californialighthouse.org](mailto:editorial@californialighthouse.org),
to send us pitches for new stories and graphics, or corrections to existing
publications.
